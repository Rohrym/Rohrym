
<div id="header" align="center">
<img src="https://media.giphy.com/media/6ib6KPmkeAjDTxMxij/giphy.gif" width="120px"/>c
</div>

<div id="header" align="center">
  <div id="badges">
    <a href="https://www.linkedin.com/in/thomashoekstra2310/">
      <img src="https://img.shields.io/badge/LinkedIn-0077B5?style=for-the-badge&logo=linkedin&logoColor=white" alt="LinkedIn Badge"/>
    </a>
    <a href="mailto:<t.hoekstra@qquest.com>?subject=Came%20from%20Github"><img src="https://img.shields.io/badge/Work_Email-%23D14836.svg?&style=for-the-badge&logo=gmail&logoColor=white" /> 
    <a href="mailto:<tthomas2310@hotmail.com>?subject=Came%20from%20Github"><img src="https://img.shields.io/badge/Personal_Email-07C160?&style=for-the-badge&logo=gmail&logoColor=white" /> 
    </a>
    <div>
      <img src="https://komarev.com/ghpvc/?username=Rohrym&style=flat-square&color=blue" alt=""/>
    </div>
  </div>
</div>

---

## Hello there!👋

My name is Thomas, and I am a [Digital Humanist](https://en.wikipedia.org/wiki/Digital_humanities) and [IT Consultant](https://en.wikipedia.org/wiki/Information_technology_consulting) working at [Qquest](https://www.qquest.nl/) based in [Utrecht](https://goo.gl/maps/vJTkQjFCtSZA9ZSN6). 

- 💼 I am currently working on several data projects.
- 🌱 I am learning PowerBI and Mendix.
- 🚀 Looking for an assignment in data or development.

### 🛠️ Languages and Frameworks in my toolbelt 🛠️

<p align="left">
<img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/python/python-original-wordmark.svg" title = "Python" alt="python" width="45" height="45" />  
<img src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/jupyter/jupyter-original-wordmark.svg" title="Jupyter" alt="Jupyter" width="45" height="45"/>
<img src="https://www.emerce.nl/content/uploads/2021/04/Mendix.png" titler="Mendix" alt="Mendix" width="45" height="45"/>
<img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/mysql/mysql-original-wordmark.svg" title="MySQL" alt="MySQL" width="45" height="45" />
<img src="https://cdn.worldvectorlogo.com/logos/java.svg" title="Java" alt="Java" width="45" height="45"/>
<img src="https://upload.wikimedia.org/wikipedia/commons/thumb/3/3f/Git_icon.svg/1024px-Git_icon.svg.png?20220905010122" title="Git" alt="Git" width="45" height="45"/>
<img src="https://pbs.twimg.com/profile_images/1329374687554646018/Fn5__gbH_400x400.png" title="oXygen" alt="XML" width="45" height="45"/>
</p>

---
